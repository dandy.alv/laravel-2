<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->only(['create','edit','update','store','destroy','index','show']);
    }
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        // dd($request->all());
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        // $query = DB::table('cast')->insert([
        //     "nama" => $request["nama"],
        //     "umur" => $request["umur"],
        //     "bio" => $request["bio"]
        // ]);

        // $cast = new Cast;
        // $cast->nama = $request['nama'];
        // $cast->umur = $request['umur'];
        // $cast->bio = $request['bio'];
        // $cast->save();

        $cast = Cast::create([
            "nama" => $request['nama'],
            "umur" => $request['umur'],
            "bio" => $request['bio']
        ]);

        return redirect('cast')->with('success', 'Cast added successfully!');
    }

    public function index(){
        // $cast = DB::table('cast')->get(); //select*from cast
        // dd($cast);
        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }

    public function show($id) {
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id);
        // dd($cast);
        return view('cast.show', compact('cast'));
    }

    public function edit($id) {
        // $cast = DB::table('cast')
        //             ->where('id', $id)
        //             ->first();
        $cast = Cast::find($id);
        // dd($cast);
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        // $query = DB::table('cast') 
        //             -> where('id', $id) 
        //             -> update([
        //                 'nama' => $request['nama'],
        //                 'umur' => $request['umur'],
        //                 'bio' => $request['bio']
        //             ]);
        $update = Cast::where('id', $id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        return redirect('/cast')->with('success', 'Data updated successfully!');
    }

    public function destroy($id){
        // $query = DB::table('cast')->where('id', $id)->delete();
        $delete = Cast::destroy($id);
        return redirect('/cast')->with('success', 'Cast successfully removed!');
    }
}
