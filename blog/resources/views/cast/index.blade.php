@extends('layouts.master')

@section('content')
<div class="card">
    <div class="card-header">
      <h3 class="card-title">Cast List</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <a class="btn btn-primary mb-3" href="{{route('cast.create')}}">Create New Cast</a>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th style="width: 10px">#</th>
                    <th>Name</th>
                    <th>Age</th>
                    <th>Bio</th>
                    <th style="width: 40px">Action</th>
                </tr>
            </thead>
        <tbody>
            @forelse ($cast as $key => $data)
                <tr>
                    <td> {{ $key + 1 }} </td>
                    <td> {{ $data->nama }} </td>
                    <td> {{ $data->umur }} </td>
                    <td> {{ $data->bio }} </td>
                    <td style="display: flex">
                        <a href="/cast/{{$data->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$data->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                        <form action="/cast/{{$data->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
                @empty
                    <tr>
                        <td colspan="4" align="center">No Cast</td>
                    </tr>    
            @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection