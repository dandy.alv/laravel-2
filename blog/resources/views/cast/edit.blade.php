@extends('layouts.master')

@section('content')
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Edit Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/cast/{{$cast->id}}" method="POST">
      @csrf
      @method('PUT')
      <div class="card-body">
        <div class="form-group">
          <label for="nama">Name</label>
          <input type="text" class="form-control" id="nama" name="nama" value="{{ $cast->nama }}" placeholder="Enter cast name">
          @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="umur">Age</label>
          <input type="number" class="form-control" id="umur" name="umur" value="{{ $cast->umur }}" placeholder="Age">
          @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        <div class="form-group">
          <label for="bio">Bio</label>
          <textarea class="form-control" rows="3" id="bio" name="bio" placeholder="Enter bio">{{$cast->bio}}</textarea>
          @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Edit</button>
      </div>
    </form>
  </div>
@endsection